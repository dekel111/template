import unittest

from values import strings
from webdriver import Driver
from pageObjects.homescreen import HomeScreen


class TestQAIdan(unittest.TestCase):

    def setUp(self):
        self.driver = Driver()
        self.driver.navigate(strings.base_url)

    def test_home_screen_components(self):
        home_screen = HomeScreen(self.driver)
        home_screen.validate_title_is_present()
        home_screen.validate_icon_is_present()
        home_screen.validate_posts_are_visible()
        pass

    def tearDown(self):
        self.driver.instance.quit()


if __name__ == '__main__':
    unittest.main()
