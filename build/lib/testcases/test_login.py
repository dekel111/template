import unittest

from testcases.dekeltestcasebase import dekeltestcasebase
from values import strings
from webdriver import Driver
from pageObjects.homescreen import HomeScreen
from pageObjects.loginscreen import Loginscreen


class TestQAIdan(dekeltestcasebase):
    def test_login(self):
        HomeScreen(self.driver).click_login()
        login_screen = Loginscreen(self.driver)
        login_screen.fill_username()
        login_screen.fill_password()
        # login_screen.click_continu()
