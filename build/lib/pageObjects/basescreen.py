import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from values import strings
import unittest


class Basescreen(unittest.TestCase):

    def __init__(self, driver):
        super().__init__()
        self.driver = driver

    def tearDown(self):
        self.driver.instance.quit()
