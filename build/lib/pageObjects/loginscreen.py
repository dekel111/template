from pageObjects.basescreen import Basescreen
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from values import strings


class Loginscreen(Basescreen):

    def __init__(self, driver):
        # super().__init__(driver)
        self.driver = driver

        self.input_id = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.ID, 'LoginInputModel_Username')))
        self.input_password = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.ID, 'LoginInputModel_Password')))

        # self.continue_button = WebDriverWait(self.driver.instance, 100).until(
        #     EC.visibility_of_element_located((
        #                                          By.CSS_SELECTOR, 'btn-info')[0]))

    def fill_username(self):
        self.input_id.send_keys('046111241')

    def fill_password(self):
        self.input_id.send_keys('12345')

    # def click_continu(self):
    #     self.continue_button.click()
