from pageObjects.basescreen import Basescreen
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from values import strings


class HomeScreen(Basescreen):

    def __init__(self, driver):
        # super(Basescreen).__init__(driver.instance)
        self.driver = driver
        self.title = WebDriverWait(self.driver.instance, 10).until(
            EC.visibility_of_element_located((
                By.CSS_SELECTOR, ".navbar-brand a")))

        self.login_button = WebDriverWait(self.driver.instance, 10).until(
            EC.visibility_of_element_located(
                (By.CLASS_NAME, "ico-user")
            ))

    def validate_title_is_present(self):
        assert self.title.is_displayed()

    def click_login(self):
        self.login_button.click()
