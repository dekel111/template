from setuptools import find_packages
from distutils.core import setup

install_requires = [
    'oauth2client',
    'apiclient',
    'requests',
    'gspread',
    'selenium',
    'pytest'
]

setup(name='Distutils',
      version='1.0',
      packages=find_packages(),
      description='automation python package',
      author='Dekel Magled',
      author_email='dekel11@gmail.com',
      install_requires=install_requires,
      classifiers=["Programming Language :: Python"]
      )
