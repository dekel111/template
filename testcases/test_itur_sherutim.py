import time
import unittest

from testcases.dekeltestcasebase import dekeltestcasebase
from values import strings
from webdriver import Driver
from pageObjects.homescreen import HomeScreen
from pageObjects.itur_sherutim import Itur_sherutim


class TestQAIdan(dekeltestcasebase):
    def test_login(self):
        # create home screen.
        home_screen = HomeScreen(self.driver)
        # call function in home screen
        # home_screen.close_pop_up_itur()
        # time.sleep(2)
        # home_screen.close_pop_up_itur()
        time.sleep(2)
        home_screen.click_itur_sherutim()

        itur_sherutim = Itur_sherutim(self.driver)
        # itur_sherutim.close_pop_up()
        itur_sherutim.fill_name()
        itur_sherutim.open_thum_sherut()
        itur_sherutim.assign_thum_sherut()
        itur_sherutim.select_thum_sherut()

    def test_failed(self):
        HomeScreen(self.driver).click_itur_sherutim()
        # login_screen.click_continu()
