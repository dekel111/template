import time
import unittest

from testcases.dekeltestcasebase import dekeltestcasebase
from values import strings
from webdriver import Driver
from pageObjects.homescreen import HomeScreen
from pageObjects.loginscreen import Loginscreen


class Test_login(dekeltestcasebase):
    def test_login(self):
        # create home screen.
        home_screen = HomeScreen(self.driver)
        # call function in home screen
        home_screen.click_login()
        # create login.
        login_screen = Loginscreen(self.driver)
        # call function in login screen
        login_screen.fill_username()
        login_screen.fill_password()
        login_screen.click_continue()
        login_screen.user_name_wrong()
        login_screen.user_name_sms()
        login_screen.birth()
        login_screen.click_continue_sms()
        login_screen.user_name_wrong_sms()
        time.sleep(5)

    def test_failed(self):
        HomeScreen(self.driver).click_login()
        # login_screen.click_continu()
