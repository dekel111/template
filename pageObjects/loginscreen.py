from pageObjects.basescreen import Basescreen
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import logging
from values import strings


class Loginscreen(Basescreen):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

        self.input_id = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.ID, 'LoginInputModel_Username')))
        self.input_password = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.ID, 'LoginInputModel_Password')))

        self.continue_button = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.XPATH,'/html/body/div/section/div/div/div/div[1]/form/div[5]/input')))

        self.input_user_name_sms = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.ID, 'OTPLoginInputModel_Username')))

        self.input_birth = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.ID, 'OTPLoginInputModel_YearOfBirth')))

        self.click_continue_sms_button = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.XPATH, '/html/body/div/section/div/div/div/div[3]/form/div[6]/input ')))

    def fill_username(self):
        self.input_id.send_keys('046111241')

    def fill_password(self):
        self.input_password.send_keys('12345')

    def click_continue(self):
        self.continue_button.click()

    def user_name_wrong(self):
        self.user_name_wrong = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.CSS_SELECTOR, '.login-form .input-holder h3')))
        assert self.user_name_wrong.is_displayed()

    def user_name_sms(self):
        self.input_user_name_sms = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.ID, 'OTPLoginInputModel_Username')))
        self.input_user_name_sms.send_keys('046111241')

    def birth(self):
        self.input_birth = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.ID, 'OTPLoginInputModel_YearOfBirth')))
        self.input_birth.send_keys('1981')

    def click_continue_sms(self):
        self.click_continue_sms_button = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.XPATH, '/html/body/div/section/div/div/div/div[3]/form/div[6]/input ')))
        self.click_continue_sms_button.click()

    def user_name_wrong_sms(self):
        self.user_name_wrong_sms = WebDriverWait(self.driver.instance, 100).until(
             EC.visibility_of_element_located((
                 By.CSS_SELECTOR, '.input-holder h3')))
        assert self.user_name_wrong_sms.is_displayed()

