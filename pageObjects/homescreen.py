from pageObjects.basescreen import Basescreen
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from values import strings


class HomeScreen(Basescreen):

    def __init__(self, driver):
        # super(Basescreen).__init__(driver.instance)
        self.driver = driver
        self.title = WebDriverWait(self.driver.instance, 10).until(
            EC.visibility_of_element_located((
                By.CSS_SELECTOR, ".navbar-brand a")))

        self.login_button = WebDriverWait(self.driver.instance, 10).until(
            EC.visibility_of_element_located(
                (By.CLASS_NAME, "ico-user")
            ))

        self.itur_sherutim = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.XPATH, '//*[@id="section-01"]/div/div/ul/li[2]/a/strong')))

        self.close_pop_up_click = WebDriverWait(self.driver.instance, 100).until(
            EC.visibility_of_element_located((
                By.XPATH, '/html/body/div[2]/div[1]/div[2]/ul/li[3]/a/img')))

    def validate_title_is_present(self):
        assert self.title.is_displayed()

    # def validate_not_title_is_present(self):
    #     assert True

    def click_login(self):
        self.login_button.click()

    # def close_pop_up_itur(self):
    #     self.close_pop_up_click.click()

    def click_itur_sherutim(self):
        self.itur_sherutim.click()
